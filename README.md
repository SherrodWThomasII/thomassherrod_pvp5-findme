# ThomasSherrod_PvP5-FindMe

Progress 11/6/18
Tested on the iPhone 8 paired with Apple Watch Series 3 - 38 mm
This build goes through contacts and simulates getting the location of a picked contact.
Just need to text the watch's location to the phone and the phone is able to get the contact.
Repo Link: https://bitbucket.org/SherrodWThomasII/thomassherrod_pvp5-findme/src/master/
